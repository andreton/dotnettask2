﻿using System;

namespace DotNetTask2
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Welcome to this application for drawing a square or rectangle");
            Console.WriteLine("Press (1) for drawing a square or (2) for drawing a rectangle");
            int userChoice; // user input for selecting drawing mode
            while (true)
            {
                if(!int.TryParse(Console.ReadLine(),out userChoice)){ //Checking for illegal input
                    Console.WriteLine("Illegal input, please try again");
                }
                else if(userChoice.Equals(1) || userChoice.Equals(2)) //user have to choose either square or rectangle
                {
                    break;
                }
                else
                {
                    Console.WriteLine("Illegal input, please try again"); //Illegal input
                }

            }

            switch (userChoice)
            {
                case 1:
                    printSquare(); //Calling the print square method
                    break;
                case 2:
                    printRectangle(); //Calling the print rectangle method 
                    break;

                default:
                    Console.WriteLine("Default"); //Standard default case switch statements
                    break;
            }
        }
        private static void printSquare() // Method for printing squares
        {
            int sidesSquare;
            Console.WriteLine("Please input the sides of the square");
            while (true)
            {
                if (!int.TryParse(Console.ReadLine(), out sidesSquare)) //Checking for correct input with parsing, it has to be a number
                {
                    Console.WriteLine("Illegal input, please try again");
                }
                else
                {
                    break;
                }
            }
            for (int i = 1; i <= sidesSquare; i++) //Loop for drawing square
            {
                for (int j = 1; j <= sidesSquare; j++)
                {
                    if (i == 1 || i == sidesSquare || j == 1 || j == sidesSquare)
                    {
                        Console.Write("#");
                    }
                    else
                    {
                        Console.Write(" ");
                    }
                }
                Console.WriteLine();

            }

        }
        private static void printRectangle() //Method for printing rectangle
        {
            int sidesHeight, sidesWidth;
            Console.WriteLine("Please enter the width of the rectangle");
            while (true)
            {
                if (!int.TryParse(Console.ReadLine(), out sidesWidth)) //Again checking for correct format on input from user
                {
                    Console.WriteLine("Illegal input, please try again");
                }
                else
                {
                    break;
                }
            }
            Console.WriteLine("Please enter the height of the rectangle"); 
            while (true)
            {
                if (!int.TryParse(Console.ReadLine(), out sidesHeight))//Checking correct input
                {
                    Console.WriteLine("Illegal input, please try again");
                }
                else
                {
                    break;
                }
            }
            for (int i = 1; i <= sidesHeight; i++) //Loop for drawing rectangle
            {
                for (int j = 1; j <= sidesWidth; j++)
                {
                    if (i == 1 || i == sidesHeight || j == 1 || j == sidesWidth)
                    {
                        Console.Write("#");
                    }
                    else
                    {
                        Console.Write(" ");
                    }
                }
                Console.WriteLine();

            }

        }
    }
}
